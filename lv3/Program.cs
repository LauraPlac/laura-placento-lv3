﻿using System;
using System.ComponentModel;

namespace lv3
{
    class Program
    {
        static void Main(string[] args)
        {

            //Z1.
            Dataset dataset = new Dataset(@"C:\Users\Laura\source\repos\lv3\CSV.txt");
            Dataset prototype = (Dataset)dataset.Clone();
            Console.WriteLine(dataset);
            Console.WriteLine(prototype);


            //Z2.
            Generator generator = Generator.GetInstance();
            int rows = 5;
            int columns = 3;
            double[][] matrix = generator.GenerateMatirx(rows, columns);

            for (int i = 0; i < rows; i++)
            {
                for (int j = 0; j < columns; j++)
                {
                    Console.Write(matrix[i][j] + "\t ");
                }
                Console.Write("\n");
            }


            //Z3.
            Logger logger = Logger.GetInstance();
            logger.Log("Hello world!");
            Logger logger1 = Logger.GetInstance();
            logger1.Log("Hi.");
            logger1.FilePath = "FileLogger2.txt";
            logger1.Log("This is from logger1");
            logger.Log("This is from logger");


            //Z4.
            NotificationManager notificationManager = new NotificationManager();
            ConsoleNotification consoleNotification = new ConsoleNotification("Laura", "Labosi", "lv3", DateTime.Now, Category.INFO, ConsoleColor.DarkRed);
            notificationManager.Display(consoleNotification);

            //Z5.
            NotificationBuilder notificationBuilder = new NotificationBuilder();
            notificationBuilder.SetAuthor("Laura").SetColor(ConsoleColor.DarkBlue).SetLevel(Category.ALERT);
            ConsoleNotification consoleNotification1 = notificationBuilder.Build();
            notificationManager.Display(consoleNotification1);



            //Z6.
            Director director = new Director(notificationBuilder);

            ConsoleNotification notification2 = director.DefaultInfo("Laura");
            ConsoleNotification notification1 = director.DefaultError("Laura");
            ConsoleNotification notification = director.DefaultAlert("Laura");

            notificationManager.Display(notification);
            notificationManager.Display(notification1);
            notificationManager.Display(notification2);

            //Z7.
            ConsoleNotification clonedNotification = (ConsoleNotification)notification1.Clone();
            notificationManager.Display(clonedNotification);

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv3
{
    //Z5.


    class NotificationBuilder : IBuilder

    {
        String author;
        ConsoleColor color;
        Category level;
        String text;
        DateTime time;
        String title;


        public NotificationBuilder()
        {
            this.author = "unknown author";
            this.color = ConsoleColor.Red;
            this.level = Category.ERROR;
            this.text = "default";
            this.time = DateTime.Now;
            this.title = "default";

        }


        public ConsoleNotification Build()
        {
            ConsoleNotification consoleNotification = new ConsoleNotification(author, title, text, time, level, color);
            return consoleNotification;
        }

        public IBuilder SetAuthor(string author)
        {
            this.author = author;
            return this;

        }

        public IBuilder SetColor(ConsoleColor color)
        {
            this.color = color;
            return this;
        }

        public IBuilder SetLevel(Category level)
        {
            this.level = level;
            return this;
        }

        public IBuilder SetText(string text)
        {
            this.text = text;
            return this;
        }

        public IBuilder SetTime(DateTime time)
        {
            this.time = time;
            return this;
        }

        public IBuilder SetTitle(string title)
        {
            this.title = title;
            return this;
        }
    }
}

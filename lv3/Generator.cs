﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace lv3
{

    //Z2.


    class Generator
    {
        private static Generator instance;
        private Random numberGenerator;

        private Generator(Random numberGenerator)
        {
            this.numberGenerator = numberGenerator;
        }

        public static Generator GetInstance()
        {
            if(instance == null)
            {
                instance = new Generator(new Random());
            }
            return instance;
        }

        //metoda ima 2 odgovornosti: stvaranje matrice i popunjavanje


        public double[][] GenerateMatirx (int rows, int columns)
        {
            double[][] matrix = new double[rows][];
            for(int i = 0; i < rows; i++)
            {
                matrix[i] = new double[columns];
            }

            for (int i = 0; i < rows; i++)
            {


                for (int j = 0; j < columns; j++)
                {
                    matrix[i][j] = this.numberGenerator.NextDouble();
                }
            }
            return matrix;
        }
        

    }
}

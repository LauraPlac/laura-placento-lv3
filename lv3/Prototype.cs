﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv3
{
    public interface Prototype
    {
        Prototype Clone();
    }
}
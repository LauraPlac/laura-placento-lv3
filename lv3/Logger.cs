﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace lv3
{

    //Z3.

    class Logger
    {
        private static Logger instance;
        public String FilePath { get; set; }
        private Logger()
        {
            this.FilePath = "FileLogger.txt";
        }

        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }

        public void Log(String message)
        {
            using (StreamWriter streamWriter = new StreamWriter(this.FilePath, true))
            {
                streamWriter.WriteLine(message);
            }
        }


    }
}

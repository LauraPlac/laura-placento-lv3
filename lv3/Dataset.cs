﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lv3
{
    class Dataset : Prototype
    {
        private List<List<string>> data; //list of lists of strings
        public Dataset()
        {
            this.data = new List<List<string>>();
        }
        public Dataset(string filePath) : this()
        {
            this.LoadDataFromCSV(filePath);
        }
        public void LoadDataFromCSV(string filePath)
        {
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    List<string> row = new List<string>();
                    string[] items = line.Split(',');
                    foreach (string item in items)
                    {
                        row.Add(item);
                    }
                    this.data.Add(row);
                }
            }
        }
        public IList<List<string>> GetData()
        {
            return
           new System.Collections.ObjectModel.ReadOnlyCollection<List<string>>(data);
        }
        public void ClearData()
        {
            this.data.Clear();
        }

        //Z1.
        public Dataset(Dataset dataset)
        {
            this.data = new List<List<string>>();
            foreach (var datasetList in dataset.GetData())
            {
                List<string> newDatasetList = new List<string>();
                foreach (var data in datasetList)
                {
                    newDatasetList.Add(data);
                }
                data.Add(newDatasetList);
            }
        }



            //potrebno je duboko kopirati zato sto koristimo liste i listu lista, liste su referencirani tip objekta

        public Prototype Clone()
        {
            return (Prototype)new Dataset(this);
        }

        //Z1.
        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var datasetList in this.GetData())
            {
                foreach (var data in datasetList)
                {
                    stringBuilder.Append(data).Append(", ");
                }
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString();
            

            

            
        }


    }
}

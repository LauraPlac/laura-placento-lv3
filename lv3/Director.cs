﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lv3
{

    //Z6.
    class Director
    {
        IBuilder builder;


        public Director(IBuilder builder)
        {
            this.builder = builder;
        }

        public ConsoleNotification DefaultInfo(string author)
        {
            builder.SetAuthor(author).SetLevel(Category.INFO).SetText("This is info");
            return builder.Build();
        }


        public ConsoleNotification DefaultError(string author)
        {
            builder.SetAuthor(author).SetLevel(Category.ERROR).SetText("Error").SetColor(ConsoleColor.Green).SetTitle("This is error");
            return builder.Build();
        }

        public ConsoleNotification DefaultAlert(string author)
        {
            builder.SetAuthor(author).SetLevel(Category.ALERT).SetTitle("Alert!").SetColor(ConsoleColor.Yellow);
            return builder.Build();
        }

    }
}
